package tests;

import funcs.Cos;
import funcs.Sin;
import interfaces.ICos;
import interfaces.ISin;
import mocks.SinMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class CosTest {
    public final static double epsilon = 0.0001;
    private static ISin sin;
    private static ICos cos;
    @BeforeAll
    private static void Init(){
        sin = new SinMock();
        cos = new Cos(sin);
    }
    @ParameterizedTest
    @ValueSource(doubles = {-0.444*Math.PI,-0.25*Math.PI, -0.75*Math.PI,-0.81*Math.PI})
    public void UpperTest(double expectedX){
        Assertions.assertEquals(Math.cos(expectedX), cos.cos(expectedX, epsilon), epsilon);
    }
    @ParameterizedTest
    @ValueSource(doubles = {0.75*Math.PI, 0.45*Math.PI, 0.33*Math.PI, 0.1*Math.PI, 0.7*Math.PI})
    public void LesserTest(double expectedX){
        Assertions.assertEquals(Math.cos(expectedX), cos.cos(expectedX, epsilon), epsilon);

    }
    @ParameterizedTest
    @ValueSource(doubles = {0.664*Math.PI, -0.25*Math.PI, Math.PI, 0.111*Math.PI})
    public void RepeatTest(double expectedX){
        Assertions.assertEquals(Math.cos(expectedX), cos.cos(expectedX, epsilon), epsilon);
        Assertions.assertEquals(cos.cos(expectedX, epsilon),cos.cos(expectedX+2*Math.PI, epsilon), epsilon);
        Assertions.assertEquals(cos.cos(expectedX, epsilon),cos.cos(expectedX-2*Math.PI, epsilon), epsilon);

    }
    @Test
    public void ZeroTest(){
        double expectedX = 0;
        Sin bs = new Sin();
        Assertions.assertEquals(Math.cos(expectedX), cos.cos(expectedX, epsilon), epsilon);
    }
    @ParameterizedTest
    @ValueSource(doubles = {0, Math.PI, -Math.PI, 0.5*Math.PI,-1.5*Math.PI})
    public void BoundTest(double expectedX){
        Sin bs = new Sin();
        Assertions.assertEquals(Math.cos(expectedX), cos.cos(expectedX, epsilon), epsilon);
    }

}
