package tests;

import funcs.Cos;
import funcs.Sec;
import funcs.Sin;
import interfaces.ICos;
import interfaces.ISec;
import interfaces.ISin;
import mocks.CosMock;
import mocks.SinMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class SecTest {
    public final static double epsilon = 0.0001;
    private static ICos cos;
    private static ISec sec;
    @BeforeAll
    private static void Init(){
        cos = new CosMock();
        sec = new Sec(cos);
    }
    @ParameterizedTest
    @ValueSource(doubles = {-0.444*Math.PI,-0.25*Math.PI, -0.75*Math.PI,-0.81*Math.PI})
    public void UpperTest(double expectedX){
        Assertions.assertEquals(1.0/Math.cos(expectedX), sec.sec(expectedX, epsilon), epsilon);
    }
    @ParameterizedTest
    @ValueSource(doubles = {0.75*Math.PI, 0.45*Math.PI, 0.33*Math.PI, 0.1*Math.PI, 0.7*Math.PI})
    public void LesserTest(double expectedX){
        Assertions.assertEquals(1.0/Math.cos(expectedX), sec.sec(expectedX, epsilon), epsilon);

    }
    @ParameterizedTest
    @ValueSource(doubles = {0.664*Math.PI, -0.33*Math.PI, Math.PI, 0.111*Math.PI, Math.PI*0.25})
    public void RepeatTest(double expectedX){
        Assertions.assertEquals(1.0/Math.cos(expectedX), sec.sec(expectedX, epsilon), epsilon);
        Assertions.assertEquals(sec.sec(expectedX, epsilon),sec.sec(expectedX+2*Math.PI, epsilon), epsilon);
        Assertions.assertEquals(sec.sec(expectedX, epsilon),sec.sec(expectedX-2*Math.PI, epsilon), epsilon);

    }
    @Test
    public void ZeroTest(){
        double expectedX = 0;
        Sin bs = new Sin();
        Assertions.assertEquals(1.0/Math.cos(expectedX), sec.sec(expectedX, epsilon), epsilon);
    }
    @ParameterizedTest
    @ValueSource(doubles = {0, Math.PI, -Math.PI, 0.5*Math.PI,-1.5*Math.PI})
    public void BoundTest(double expectedX){
        Sin bs = new Sin();
        Assertions.assertEquals(1.0/Math.cos(expectedX), sec.sec(expectedX, epsilon), epsilon);
    }
}
