package tests;

import funcs.*;
import interfaces.*;
import mocks.CosMock;
import mocks.CotMock;
import mocks.LogMock;
import mocks.SecMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

public class FuncSystemTest {

    public final static double epsilon = 0.0001;
    static IFuncSystem sys;
    static ICos iCos;
    static ISec iSec;
    static ICot iCot;
    static ILog iLog;
    @BeforeAll
    public static void InitSystem(){
        iCos = new CosMock();
        iSec = new SecMock();
        iCot = new CotMock();
        iLog = new LogMock();
        sys = new LabFunction(iCos, iLog, iSec, iCot);
    }
    public static List<Double> generateRoots(){
        List<Double> roots = new ArrayList<>();
        roots.add(1.0);
        for(int i = -8; i < -1; i++){
            roots.add(2*(Math.PI * i + 0.427734));
            roots.add(2*(Math.PI * i + 1.14306));
        }
        return roots;
    }
    @ParameterizedTest
    @MethodSource("generateRoots")
    public void rootTest(double expectedX){
        Assertions.assertEquals(0.000, sys.calculate(expectedX, epsilon), epsilon);
    }
    @ParameterizedTest
    @MethodSource("generateRoots")
    public void unMocked(double expectedX){
        Sin sin = new Sin();
        Ln ln = new Ln();
        Cos cos = new Cos(sin);
        Sec sec = new Sec(cos);
        Cot cot = new Cot(sin, cos);
        Log log = new Log(ln);

        IFuncSystem test = new LabFunction(cos, log, sec, cot);
        Assertions.assertEquals(0.000, sys.calculate(expectedX, epsilon), epsilon);
    }

}
