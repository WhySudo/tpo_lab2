package tests;

import funcs.Ln;
import funcs.Log;
import interfaces.ILn;
import interfaces.ILog;
import mocks.LnMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class LogTest {
    static ILn ln;
    static ILog log;
    public final static double epsilon = 0.0000001;
    @BeforeAll
    public static void BeforeTest() {
        ln = new LnMock();
        log = new Log(ln);
    }

    @ParameterizedTest
    @ValueSource(doubles = {1.22, 3.49, 5.555, 100.00, 0.1, 0.99, 0.3, 0.5, 1.0})
    public void Test5Base(double expectedX) {
        Assertions.assertEquals(Math.log(expectedX)/Math.log(5), log.log(expectedX, 5, epsilon), epsilon*1000);
    }
    @ParameterizedTest
    @ValueSource(doubles = {1.22, 3.49, 5.555, 100.00, 0.1, 0.99, 0.3, 0.5, 1.0})
    public void Test2Base(double expectedX) {
        Assertions.assertEquals(Math.log(expectedX)/Math.log(2), log.log(expectedX, 2, epsilon), epsilon*1000);
    }
    @ParameterizedTest
    @ValueSource(doubles = {1.22, 3.49, 5.555, 100.00, 0.1, 0.99, 0.3, 0.5, 1.0})
    public void Test10Base(double expectedX) {
        Assertions.assertEquals(Math.log10(expectedX), log.log(expectedX, 10, epsilon), epsilon*1000);
    }
    @ParameterizedTest
    @ValueSource(doubles = {1.22, 3.49, 5.555, 100.00, 0.1, 0.99, 0.3, 0.5, 1.0})
    public void ConversationToBase(double expectedX) {

        //log_10(x) = log_2(x)/log_2(10)
        Assertions.assertEquals(Math.log10(expectedX), log.log(expectedX, 2, epsilon)/log.log(10, 2, epsilon), epsilon*1000);
    }

}
