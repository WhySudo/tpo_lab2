package tests;

import funcs.Cos;
import funcs.Sin;
import interfaces.ICos;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.mockito.Mockito.*;

public class SinTest {
    public final static double epsilon = 0.0001;
    @ParameterizedTest
    @ValueSource(doubles = {0.444*Math.PI, 0.25*Math.PI, 0.5*Math.PI,-0.32*Math.PI})
    public void UpperTest(double expectedX){
        Sin bs = new Sin();
        Assertions.assertEquals(Math.sin(expectedX), bs.sin(expectedX, epsilon), epsilon);
    }
    @ParameterizedTest
    @ValueSource(doubles = {0.75*Math.PI, 1.25*Math.PI, Math.PI, 1.5*Math.PI, 1.33*Math.PI})
    public void LesserTest(double expectedX){
        Sin bs = new Sin();
        Assertions.assertEquals(Math.sin(expectedX), bs.sin(expectedX, epsilon), epsilon);

    }
    @ParameterizedTest
    @ValueSource(doubles = {0.664*Math.PI, -0.25*Math.PI, Math.PI, 0.111*Math.PI})
    public void RepeatTest(double expectedX){
        Sin bs = new Sin();
        Assertions.assertEquals(Math.sin(expectedX), bs.sin(expectedX, epsilon), epsilon);
        Assertions.assertEquals(bs.sin(expectedX, epsilon),bs.sin(expectedX+2*Math.PI, epsilon), epsilon);
        Assertions.assertEquals(bs.sin(expectedX, epsilon),bs.sin(expectedX-2*Math.PI, epsilon), epsilon);

    }
    @Test
    public void ZeroTest(){
        double expectedX = 0;
        Sin bs = new Sin();
        Assertions.assertEquals(Math.sin(expectedX), bs.sin(expectedX, epsilon), epsilon);
    }
    @ParameterizedTest
    @ValueSource(doubles = {0, Math.PI, -Math.PI, 0.5*Math.PI,-1.5*Math.PI})
    public void BoundTest(double expectedX){
        Sin bs = new Sin();
        Assertions.assertEquals(Math.sin(expectedX), bs.sin(expectedX, epsilon), epsilon);
    }

}
