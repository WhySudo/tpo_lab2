package tests;

import funcs.Cos;
import funcs.Cot;
import funcs.Sin;
import interfaces.ICos;
import interfaces.ICot;
import interfaces.ISin;
import mocks.CosMock;
import mocks.SinMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class CotTest {
    public final static double epsilon = 0.0001;
    private static ISin sin;
    private static ICos cos;
    private static ICot cot;

    @BeforeAll
    private static void Init() {
        sin = new SinMock();
        cos = new CosMock();
        cot = new Cot(sin, cos);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.444 * Math.PI, 0.25 * Math.PI, 0.5 * Math.PI, -0.32 * Math.PI})
    public void UpperTest(double expectedX) {
        Assertions.assertEquals(1.0/Math.tan(expectedX), cot.cot(expectedX, epsilon), epsilon);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.75 * Math.PI, 1.25 * Math.PI, Math.PI, 1.5 * Math.PI, 1.33 * Math.PI})
    public void LesserTest(double expectedX) {
        Assertions.assertEquals(1.0/Math.tan(expectedX), cot.cot(expectedX, epsilon), epsilon);
    }

    //TODO: Math.PI Неопределенное
    @ParameterizedTest
    @ValueSource(doubles = {0.664 * Math.PI, -0.25 * Math.PI, 0.111 * Math.PI})
    public void RepeatTest(double expectedX) {

        Assertions.assertEquals(1.0/Math.tan(expectedX), cot.cot(expectedX, epsilon), epsilon);
        Assertions.assertEquals(cot.cot(expectedX, epsilon), cot.cot(expectedX + Math.PI, epsilon), epsilon);
        Assertions.assertEquals(cot.cot(expectedX, epsilon), cot.cot(expectedX - Math.PI, epsilon), epsilon);

    }

    @Test
    public void ZeroTest() {
        double expectedX = 0;
        Assertions.assertEquals(1.0/Math.tan(expectedX), cot.cot(expectedX, epsilon), epsilon);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, Math.PI, -Math.PI, 0.5 * Math.PI, -1.5 * Math.PI})
    public void BoundTest(double expectedX) {
        Assertions.assertEquals(1.0/Math.tan(expectedX), cot.cot(expectedX, epsilon), epsilon);
    }

}
