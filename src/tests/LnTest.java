package tests;

import funcs.Ln;
import interfaces.ILn;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class LnTest {
    static ILn ln;
    public final static double epsilon = 0.0000001;
    @BeforeAll
    public static void BeforeTest() {
        ln = new Ln();
    }

    @Test
    public void SolutionTest() {
        double expectedX = 1;
        Assertions.assertEquals(Math.log(expectedX), ln.ln(expectedX, epsilon), epsilon*1000);
    }
    @ParameterizedTest
    @ValueSource(doubles = {1.22, 3.49, 5.555, 100.00})
    public void UpperTest(double expectedX) {
        Assertions.assertEquals(Math.log(expectedX), ln.ln(expectedX, epsilon), epsilon*1000);
    }
    @ParameterizedTest
    @ValueSource(doubles = {0.88, 0.10, 0.55, 0.81})
    public void LowerTest(double expectedX) {
        Assertions.assertEquals(Math.log(expectedX), ln.ln(expectedX, epsilon), epsilon*1000);

    }
    @ParameterizedTest
    @ValueSource(doubles = {0.88, 0.10, 0.55, 0.81})
    public void XSolutionTest(double expectedX) {
        double value = ln.ln(expectedX, epsilon);
        Assertions.assertEquals(Math.pow(Math.E, value), expectedX, epsilon*1000);
    }
}
