package mocks;

import interfaces.ICot;

public class CotMock implements ICot {
    @Override
    public double cot(double x, double eps) {
        return 1.0/ Math.tan(x);
    }
}
