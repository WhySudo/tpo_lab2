package mocks;

import interfaces.ICos;

public class CosMock implements ICos {
    @Override
    public double cos(double x, double eps) {
        return Math.cos(x);
    }
}
