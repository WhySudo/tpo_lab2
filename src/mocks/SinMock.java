package mocks;

import interfaces.ISin;

public class SinMock implements ISin {
    @Override
    public double sin(double x, double eps) {
        return Math.sin(x);
    }
}
