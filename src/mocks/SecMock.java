package mocks;

import interfaces.ISec;

public class SecMock implements ISec {
    @Override
    public double sec(double x, double eps) {
        return 1.0/Math.cos(x);
    }
}
