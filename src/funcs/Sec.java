package funcs;

import interfaces.ICos;
import interfaces.ISec;

public class Sec implements ISec {

    private ICos iCos;

    public Sec(ICos iCos) {
        this.iCos = iCos;
    }

    @Override
    public double sec(double x, double eps) {
        return 1/iCos.cos(x, eps);
    }
}
