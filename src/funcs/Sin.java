package funcs;

import interfaces.ISin;

import java.lang.Math;
public class Sin implements ISin {
    public double sin(double x, double eps){
        double accFact = 3 * 2;
        double prev = x;
        double res = x - (Math.pow(x, 3)/accFact);
        double tMem;
        int counter = 3;
        while(Math.abs(prev-res) >= eps){
            counter+=2;
            accFact *= ((counter)*(counter-1));
            prev = res;
            tMem = Math.pow(x, counter)/accFact;
            if(counter % 4 == 3){
                tMem *= (-1);

            }
            res += tMem;
            //System.out.printf("%f - %f = %f\n", res, prev, Math.abs(prev-res));
        }
        return res;
    }

}
