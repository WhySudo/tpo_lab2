package funcs;

import interfaces.ICos;
import interfaces.ICot;
import interfaces.ISin;

public class Cot implements ICot {
    private ISin iSin;
    private ICos iCos;

    public Cot(ISin iSin, ICos iCos) {
        this.iSin = iSin;
        this.iCos = iCos;
    }

    @Override
    public double cot(double x, double eps) {
        return iCos.cos(x, eps)/iSin.sin(x, eps);
    }
}
