package funcs;

import interfaces.ILn;

public class Ln implements ILn {
    public double ln(double x, double epsilon) {
        //epsilon *= 0.1;
        double arg = (x-1)/(x+1);
        double res = arg;
        double member = arg;
        double prev_res;
        int counter = 3;
        do{
            prev_res = member;
            member = Math.pow(arg, counter)/(double) counter;
            res += member;
            counter += 2;
        }while(2*Math.abs(prev_res-member) >= epsilon);
        return 2*res;
        //0.0001
        //0.0002
        //0.00029
        //0.00038
        //0.00010
        //0.00009
        //0.00009
    }
}
