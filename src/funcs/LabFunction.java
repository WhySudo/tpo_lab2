package funcs;

import interfaces.*;

public class LabFunction implements IFuncSystem {
    private ICos iCos;
    private ILog iLog;
    private ISec iSec;
    private ICot iCot;

    public LabFunction(ICos iCos, ILog iLog, ISec iSec, ICot iCot) {
        this.iCos = iCos;
        this.iLog = iLog;
        this.iSec = iSec;
        this.iCot = iCot;
    }

    @Override
    public double calculate(double x, double eps) {
        if(x <= 0){
            return (iCot.cot(x, eps) - iSec.sec(x, eps)) + iCos.cos(x, eps);
        }
        double express = iLog.log(x, 2, eps) + iLog.log(x, 2, eps);
        express *= iLog.log(x, 5, eps);
        express *= iLog.log(x, 10, eps);
        express = Math.pow(express, 3);
        express = Math.pow(express, 2);
        return express;
    }
}
