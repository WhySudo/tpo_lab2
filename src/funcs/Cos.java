package funcs;

import interfaces.ICos;
import interfaces.ISin;

public class Cos implements ICos {

    private ISin iSin;

    public Cos(ISin iSin) {
        this.iSin = iSin;
    }

    @Override
    public double cos(double x, double eps) {
        return iSin.sin(x+(Math.PI/2), eps);
    }
}
