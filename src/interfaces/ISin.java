package interfaces;

public interface ISin {
    double sin(double x, double eps);
}
