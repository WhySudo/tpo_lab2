package interfaces;

public interface ISec {
    double sec(double x, double eps);
}
