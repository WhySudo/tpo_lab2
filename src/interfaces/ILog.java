package interfaces;

public interface ILog {
    double log(double x, double a, double eps);
}
