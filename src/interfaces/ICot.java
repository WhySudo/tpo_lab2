package interfaces;

public interface ICot {
    double cot(double x, double eps);
}
