package interfaces;

public interface ICos {
    double cos(double x, double eps);
}
