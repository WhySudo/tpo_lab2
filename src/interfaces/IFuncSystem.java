package interfaces;

public interface IFuncSystem {
    double calculate(double x, double eps);
}
